﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggle_light : MonoBehaviour {

    public Light light;
    public GameController game_controller;
	// Use this for initialization
	void Start () {
        game_controller = GetComponent<GameController>();
    }
	
	// Update is called once per frame
	void Update () { 

    }

    public void Toggle()
    {
        Camera.main.GetComponent<AudioController>().PlayLighthouseToggle();

        if (light.enabled)
        {
            light.enabled = false;
            game_controller.lighthouse_active = false;
        }
        else
        {
            light.enabled = true;

            game_controller.lighthouse_active = true;
        }
    }
}
