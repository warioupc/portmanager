﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForkliftSalary : MonoBehaviour {

    Text text;
    Resource_manager_script resource_manager;
    BuyForklift buy_worker;


    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
        resource_manager = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
        buy_worker = GameObject.FindGameObjectWithTag("BuyForklift").GetComponent<BuyForklift>();
    }

    // Update is called once per frame
    void Update()
    {
        string _text = "Total forklift salary: ";
        int salary = resource_manager.forklift_salary * buy_worker.num_of_forklifts();
        print(salary);
        text.text = string.Concat(_text, salary.ToString(), "$");
    }
}