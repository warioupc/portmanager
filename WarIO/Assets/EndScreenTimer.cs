﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenTimer : MonoBehaviour {

    Text text;
    GameController game_controller;
	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        game_controller = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
        string display = "You lasted: ";
        string time = game_controller.getTime();
        text.text = string.Concat(display, time);
	}
}
