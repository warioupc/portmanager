﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkerSalary : MonoBehaviour {

    Text text;
    Resource_manager_script resource_manager;
    BuyWorker buy_worker;
    

    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
        resource_manager = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
        buy_worker = GameObject.FindGameObjectWithTag("BuyWorker").GetComponent<BuyWorker>();
    }

    // Update is called once per frame
    void Update () {
        string _text = "Total worker salary: ";
        int salary = resource_manager.worker_salary * buy_worker.num_of_workers();
        print(salary);
        text.text = string.Concat(_text, salary.ToString(), "$");
	}
}
