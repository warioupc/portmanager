﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairBoat : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void AssignWorkers()
    {
        GameObject[] boats = GameObject.FindGameObjectsWithTag("Boat");
        GameObject[] workers = GameObject.FindGameObjectsWithTag("Worker");
        for(int i =0; i < boats.Length; ++i)
        {
            if (boats[i].GetComponent<AgentController>().is_broken)
            {
                Camera.main.GetComponent<AudioController>().PlayForkliftSound();

                boats[i].GetComponent<AgentController>().home.GetComponentInParent<Dock_Controller>().needs_repair = true;
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().Money -= 10;
            }
        }

    }
}
