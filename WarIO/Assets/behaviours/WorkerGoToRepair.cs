using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NodeCanvas.Tasks.Actions{

	public class WorkerGoToRepair : ActionTask{

        SteeringPathfinding steering_pathfinding;
        WorkerController worker_controller;
        Move move;
		//Use for initialization. This is called only once in the lifetime of the task.
		//Return null if init was successfull. Return an error string otherwise
		protected override string OnInit(){
            steering_pathfinding = agent.GetComponent<SteeringPathfinding>();
            worker_controller = agent.GetComponent<WorkerController>();
            move = agent.GetComponent<Move>();
            move.target = worker_controller.home;
			return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            if (move.target.transform.position != Vector3.zero)
            {
                move.target = worker_controller.target;
                steering_pathfinding.enabled = true;
                steering_pathfinding.generate_path = true;
            }
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			if(steering_pathfinding.finished)
            {
                EndAction();
            }
		}

		//Called when the task is disabled.
		protected override void OnStop(){
            steering_pathfinding.enabled = false;
		}

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}