using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NodeCanvas.Tasks.Actions{

	public class Activate_Pathfinding : ActionTask{

        SteeringPathfinding pathfinding;
		//Use for initialization. This is called only once in the lifetime of the task.
		//Return null if init was successfull. Return an error string otherwise
		protected override string OnInit(){
            pathfinding = agent.GetComponent<SteeringPathfinding>();
			return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            pathfinding.generate_path = true;
            pathfinding.enabled = true;

          //  Debug.Log(pathfinding.path);


		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			if(pathfinding.finished)
            {
                Debug.Log("pathfinding finished");
                pathfinding.enabled = false;
                pathfinding.finished = false;
                EndAction(true);
            }
		}

		//Called when the task is disabled.
		protected override void OnStop(){
            pathfinding.enabled = false;
        }

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}