using NodeCanvas.Framework;
using ParadoxNotion.Design;


namespace NodeCanvas.Tasks.Actions{

	public class Boat_Deliver_Fish : ActionTask{

        AgentController agent_controller;
        ChangeSprite change_sprite;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit(){
            agent_controller = agent.GetComponent<AgentController>();
            change_sprite = agent.GetComponent<ChangeSprite>();

            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            agent_controller.has_fish = false;
            agent_controller.home.GetComponentInParent<Dock_Controller>().has_materials = true;
            agent_controller.home.GetComponentInParent<Dock_Controller>().needs_forklift = true;
            change_sprite.SetSprite_none();
            EndAction();
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			
		}

		//Called when the task is disabled.
		protected override void OnStop(){
			
		}

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}