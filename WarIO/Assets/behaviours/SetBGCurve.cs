using NodeCanvas.Framework;
using ParadoxNotion.Design;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

namespace NodeCanvas.Tasks.Actions{

	public class SetBGCurve : ActionTask{

        SteeringFollowPath follow_path;
        Move move;
		//Use for initialization. This is called only once in the lifetime of the task.
		//Return null if init was successfull. Return an error string otherwise
		protected override string OnInit(){
            follow_path = agent.GetComponent<SteeringFollowPath>();
            move = agent.GetComponent<Move>();
            
            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            if (move.target.name == "fishing path 1")
                follow_path.path = follow_path.path1;

            else if (move.target.name == "fishing path 2")
                follow_path.path = follow_path.path2;
            EndAction();
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			
		}

		//Called when the task is disabled.
		protected override void OnStop(){
			
		}

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}