using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NodeCanvas.Tasks.Actions{

	public class TryToBreak : ActionTask{

        AgentController agent_controller;
        ChangeSprite change_sprite;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit(){
            agent_controller = agent.GetComponent<AgentController>();
            change_sprite = agent.GetComponent<ChangeSprite>();
            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            if (Random.Range(0, 100) <= agent_controller.chance_of_breaking)
            {
                change_sprite.SetSprite_none();
                agent_controller.is_broken = true;
               
                EndAction(false);
            }
            else
                EndAction(true);
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			
		}

		//Called when the task is disabled.
		protected override void OnStop(){
			
		}

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}