using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NodeCanvas.Tasks.Actions{

	public class TryToFish : ActionTask{

        ChangeSprite change_sprite;
        AgentController agent_controller;
        public float time_between_fishing_attempts = 10.0f;
        float current_time = 0.0f;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit()
        {
            agent_controller = agent.GetComponent<AgentController>();
            change_sprite = agent.GetComponent<ChangeSprite>();

            return null;
        }

        //This is called once each time the task is enabled.
        //Call EndAction() to mark the action as finished, either in success or failure.
        //EndAction can be called from anywhere.
        protected override void OnExecute(){

		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
            current_time += Time.deltaTime;
            if (current_time >= time_between_fishing_attempts)
            {
                if (Random.Range(0, 101) <= agent_controller.chance_of_fishing)
                {
                    agent_controller.has_fish = true;
                    change_sprite.SetSprite_hasfish();
                }
                current_time = 0;
            }
        }

		//Called when the task is disabled.
		protected override void OnStop(){
            current_time = 0;
            change_sprite.SetSprite_none();
        }

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}