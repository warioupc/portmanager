using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using System.Collections;


namespace NodeCanvas.Tasks.Actions{

	public class Activate_FollowPath : ActionTask{

        SteeringFollowPath follow_path;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit(){
            follow_path = agent.GetComponent<SteeringFollowPath>();
 
            return null;
        }

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            follow_path.enabled = true;
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			
		}

		//Called when the task is disabled.
		protected override void OnStop(){
            follow_path.enabled = false;
        }

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}