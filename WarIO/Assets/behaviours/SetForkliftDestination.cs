using NodeCanvas.Framework;
using ParadoxNotion.Design;



namespace NodeCanvas.Tasks.Actions{

	public class SetForkliftDestination : ActionTask{

        ForkliftController forklift_controller;
        Move move;
        DisplayStateWorker change_sprite;
        
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit(){
            forklift_controller = agent.GetComponent<ForkliftController>();
            move = agent.GetComponent<Move>();
            change_sprite = agent.GetComponent<DisplayStateWorker>();
            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            if (forklift_controller.assigned)
            {
                if (!forklift_controller.has_materials)
                {
                    move.target = forklift_controller.dock_assigned_to;
                    change_sprite.SetSprite_isreparing();
                }
                else
                {
                    move.target = forklift_controller.deliver_area;
                    change_sprite.SetSprite_none();
                }
            }
			EndAction(true);
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			
		}

		//Called when the task is disabled.
		protected override void OnStop(){
			
		}

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}