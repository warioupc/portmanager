using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace NodeCanvas.Tasks.Actions{

	public class SetPathfindingDestination : ActionTask{

        Move move;
        SteeringPathfinding pathfinding;
        GameObject target;
        GameObject target2;
        ChangeSprite change_sprite;
		//Use for initialization. This is called only once in the lifetime of the task.
		//Return null if init was successfull. Return an error string otherwise
		protected override string OnInit(){
            move = agent.GetComponent<Move>();
            pathfinding = agent.GetComponent<SteeringPathfinding>();
            target = GameObject.FindGameObjectWithTag("fishing path 1");
            target2 = GameObject.FindGameObjectWithTag("fishing path 2");
            change_sprite = agent.GetComponent<ChangeSprite>();
            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            change_sprite.SetSprite_none();
            pathfinding.enabled = true;
            if (Random.Range(0, 2) == 0)
            {
                move.target = target;
            }
            else
            {
                move.target = target2;
            }
            pathfinding.generate_path = true;
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
            if (pathfinding.finished)
            {
                change_sprite.SetSprite_isfishing();
                pathfinding.finished = false;
                pathfinding.enabled = false;
                EndAction(true);
            }
        }

		//Called when the task is disabled.
		protected override void OnStop(){
            change_sprite.SetSprite_none();
            pathfinding.enabled = false;
            pathfinding.finished = false;
        }

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}