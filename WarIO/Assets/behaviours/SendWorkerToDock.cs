﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendWorkerToDock : MonoBehaviour {

    public void SendWorker()
    {
        GameObject[] workers = GameObject.FindGameObjectsWithTag("Worker");
        int i = Random.Range(0, workers.Length - 1);
        workers[i].GetComponent<WorkerController>().target = this.gameObject;
        workers[i].GetComponent<Move>().target = this.gameObject;
        workers[i].GetComponent<WorkerController>().assigned = true;

    }
}
