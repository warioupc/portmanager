using NodeCanvas.Framework;
using ParadoxNotion.Design;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NodeCanvas.Tasks.Actions{

	public class GoToHome : ActionTask{

        AgentController agent_controller;
        SteeringPathfinding steering_pathfinding;
        Move move;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit(){
            agent_controller = agent.GetComponent<AgentController>();
            move = agent.GetComponent<Move>();
            steering_pathfinding = agent.GetComponent<SteeringPathfinding>();
            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){
            move.target = agent_controller.home;
            steering_pathfinding.enabled = true;
            steering_pathfinding.generate_path = true; ;
            
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
            if (steering_pathfinding.finished)
                EndAction(true);
		}

		//Called when the task is disabled.
		protected override void OnStop(){
            steering_pathfinding.finished = false;
            steering_pathfinding.enabled = false;
        }

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}