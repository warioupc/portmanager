﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_FollowButton : MonoBehaviour {

    public Button button;
    public bool active = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (active)
            button.transform.position = this.transform.position;
        if (!active)
        {
            Vector3 t; t.x = -10000; t.y = -10000;t.z = -100000;
            button.transform.Translate(t);
        }

        
	}
}
