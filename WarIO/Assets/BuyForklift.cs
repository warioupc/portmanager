﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyForklift : MonoBehaviour {

    public GameObject[] forklifts;
    Resource_manager_script resource_manager;
    public int price;
	// Use this for initialization
	void Start () {
        resource_manager = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
        forklifts = GameObject.FindGameObjectsWithTag("Forklift");
        for(int i = 1; i<forklifts.Length;i++)
        {
            forklifts[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Buy_Forklift()
    {
        if(resource_manager.Money >= price)
        {
            for (int i = 0; i<forklifts.Length;i++)
            {
                if (forklifts[i].activeSelf == false)
                {
                    Camera.main.GetComponent<AudioController>().PlayForkliftSound();
                    forklifts[i].SetActive(true);
                    resource_manager.Money -= price;
                    break;
                }
            }
        }
    }

    public int num_of_forklifts()
    {
        int ret = 0;
        for (int i = 0; i < forklifts.Length; i++)
        {
            if (forklifts[i].activeSelf == true)
            {
                ret++;
            }
        }
        return ret;
    }
}
