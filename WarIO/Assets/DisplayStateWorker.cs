﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStateWorker : MonoBehaviour {

    // Use this for initialization
    public Image image;

    void Start()
    {
        SetSprite_none();
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 newpos;
        newpos.x = this.transform.position.x;
        newpos.y = this.transform.position.y + 65;
        newpos.z = this.transform.position.z;
        //image.transform.position =  Vector3(this.transform.position.x, this.transform.position.y + 20, this.transform.position.z);
        image.transform.position = newpos;
    }

    public void SetSprite_isreparing()
    {
        var tempColor = image.color;
        tempColor.a = 1;
        image.color = tempColor;
    }

    public void SetSprite_none()
    {
        var tempColor = image.color;
        tempColor.a = 0;
        image.color = tempColor;
    }
}
