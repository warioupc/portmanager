﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightLights : MonoBehaviour {

    DayCycle day_cycle;
    Light _light;
	// Use this for initialization
	void Start () {
        day_cycle = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<DayCycle>();
        _light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        if (day_cycle.is_day)
        {
            _light.enabled = false;
        }
        else
            _light.enabled = true;
	}
}
