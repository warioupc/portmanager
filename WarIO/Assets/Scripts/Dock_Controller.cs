﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dock_Controller : MonoBehaviour
{

    public bool has_materials = false;
    public bool needs_forklift = false;
    public bool needs_repair = false;
    public GameObject forklift_destination;
    public GameObject Boat_assigned;
    public GameObject Boat_assigned_2;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (needs_forklift)
        {
            GameObject[] forklifts = GameObject.FindGameObjectsWithTag("Forklift");
            for (int i = 0; i < forklifts.Length; i++)
            {
                ForkliftController forklift_controller = forklifts[i].GetComponent<ForkliftController>();
                if (forklift_controller.assigned == false)
                {
                    forklift_controller.dock_assigned_to = forklift_destination;
                    print("a forklift has been assigned to:");
                    print(forklift_controller.dock_assigned_to.name);
                    forklift_controller.assigned = true;
                    needs_forklift = false;
                    break;
                }
            }
        }
        if (needs_repair)
        {
            GameObject[] workers = GameObject.FindGameObjectsWithTag("Worker");
            for (int i = 0; i < workers.Length; i++)
            {
                WorkerController worker = workers[i].GetComponent<WorkerController>();
                if (worker.assigned == false)
                {
                    worker.target = forklift_destination;
                    worker.assigned = true;
                    needs_repair = false;
                    break;
                }
            }
        }
    }

    public void RepairShip()
    {
        Boat_assigned.GetComponent<AgentController>().is_broken = false;
        Boat_assigned_2.GetComponent<AgentController>().is_broken = false;
    }

    public bool ActivateBoat()
    {
        bool ret = false;
        if(Boat_assigned.active == false)
        {
            Boat_assigned.SetActive(true);
            ret = true;
        }
        else if(Boat_assigned_2.active == false)
        {
            Boat_assigned_2.SetActive(true);
            ret = true;
        }
        return ret;
    }
}
