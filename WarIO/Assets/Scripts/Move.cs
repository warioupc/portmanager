﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Move : MonoBehaviour
{

    Vector3[] movement_velocity;
    Vector3[] movement_acceleration;
    float[] rotation_velocity;
    float[] rotation_acceleration;
    public GameObject target;
    AgentController agent_controller;
    public float max_mov_velocity = 5.0f;
    public float max_mov_acceleration = 0.1f;
    public float max_rot_velocity = 10.0f; // in degrees / second
    public float max_rot_acceleration = 0.1f; // in degrees
    public float min_rotation_angle = 0.1f;
    public bool rotating = false;
    public bool oriented = true;
    public bool broken = false;
    Animator m_Animator;

    [Header("-------- Read Only --------")]
    public Vector3 movement = Vector3.zero;
    public float rotation = 0.0f; // degrees
  
    void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
        agent_controller = gameObject.GetComponent<AgentController>();

        movement_velocity = new Vector3[SteeringConf.num_priorities+1];
        movement_acceleration = new Vector3[SteeringConf.num_priorities+1];
        rotation_velocity = new float[SteeringConf.num_priorities+1];
        rotation_acceleration = new float[SteeringConf.num_priorities+1];

        for (int i = 0; i < SteeringConf.num_priorities; i++)
        {
            movement_velocity.SetValue(Vector3.zero, i);
            movement_acceleration.SetValue(Vector3.zero, i);
            rotation_velocity.SetValue(0.0f, i);
            rotation_acceleration.SetValue(0.0f, i);
        }

       
    }
    // Methods for behaviours to set / add velocities
    public void SetMovementVelocity(Vector3 velocity, int priority)
    {
        movement_velocity[priority] += velocity;
    }

    public void AccelerateMovement(Vector3 velocity, int priority)
    {
        movement_acceleration[priority] += velocity;
    }

    public void SetRotationVelocity(float _rotation_velocity, int priority)
    {
        rotation_velocity[priority] += _rotation_velocity;
    }

    public void AccelerateRotation(float _rotation_acceleration, int priority)
    {
        rotation_acceleration[priority] += _rotation_acceleration;
    }

    Vector3 GetHighestPriorityVector3(Vector3[] array)
    {
        Vector3 ret = Vector3.zero;

        for (int i = array.Length - 1; i > 0; i--)
        {
            if (array[i].magnitude > 0.05 || array[i].magnitude < -0.05)
            {
                ret = array[i];
                break;
            }
        }

        return ret;
    }

    float GetHighestPriorityFloat(float[] array)
    {
        float ret = .0f;

        for (int i = array.Length - 1; i > 0; i--)
        {
            if (array[i] > 0.1 || array[i] < -0.1)
            {
                ret = array[i];
                break;
            }
        }

        return ret;
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 new_movement = GetHighestPriorityVector3(movement_velocity);
         movement = new_movement;
        
        Vector3 new_acceleration = GetHighestPriorityVector3(movement_acceleration);
        movement += new_acceleration;

        float new_rotation_velocity = GetHighestPriorityFloat(rotation_velocity);
        rotation = new_rotation_velocity;

        float new_rotation_acceleration = GetHighestPriorityFloat(rotation_acceleration);
        rotation += new_rotation_acceleration;
        
        //cap velocity
        if (movement.magnitude > max_mov_velocity)
        {
            movement = max_mov_velocity * movement.normalized;
        }

        // cap rotation
        Mathf.Clamp(rotation, -max_rot_velocity, max_rot_velocity);

        // final rotate
        if (!GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<GameController>().game_over)
            transform.rotation *= Quaternion.AngleAxis(rotation * Time.deltaTime, Vector3.up);

        // finally move

        //if (movement != Vector3.zero)
        //{
        //    m_Animator.SetBool("moving",true);
        //}
        //else
        //{
        //    m_Animator.SetBool("moving", false);
        //}


        Debug.DrawRay(transform.position, movement, Color.black);
        movement.y = 0.0f;

        if(!GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<GameController>().game_over)
            transform.position += movement * Time.deltaTime;

        int debug_count = 0;
        for (int i = 0; i <= SteeringConf.num_priorities; i++)
        {
            movement_velocity[i] = Vector3.zero;
            movement_acceleration[i] = Vector3.zero;
            rotation_velocity[i] = 0.0f;
            rotation_acceleration[i] = 0.0f;
            debug_count++;
        }
    }
}





































































































































































