﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveCamera : MonoBehaviour {

    public float Speed = 240.0f;

    public int zMinLimit = -307;
    public int zMaxLimit = -40;

    public int xMinLimit = -307;
    public int xMaxLimit = 50;

    private float x = 22.0f;
    private float y = 33.0f;

    public Canvas canvas;


    public void Start()
    {

    }

    public void Update()
    {
            Vector3 vec = Vector3.zero;
            if (Input.GetKey("s") && transform.position.x > xMinLimit)
            {
            vec.x-=Speed * Time.fixedDeltaTime;
            }
            if (Input.GetKey("d") && transform.position.z > zMinLimit)
            {
            vec.z -=Speed * Time.fixedDeltaTime;
            }
            if (Input.GetKey("w") && transform.position.x < xMaxLimit)
            {
            vec.x += Speed * Time.fixedDeltaTime;
            }
            if (Input.GetKey("a") && transform.position.z < zMaxLimit)
            {
            vec.z +=Speed * Time.fixedDeltaTime;
            }
        if (!GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<GameController>().game_over)
        {
            Camera.main.transform.position += vec;
            canvas.transform.position += vec;
        }
    }
}
