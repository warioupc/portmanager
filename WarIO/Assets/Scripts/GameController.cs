﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    protected bool is_day = true;
    protected bool is_changing = false;
    public bool lighthouse_active = false;
    bool accelerated_time = false;
    public float time_accelerator_multiplier = 5.0f;

    public bool game_over = false;
    public bool victory = false;

    public float timer = 0.0f;
    public int minutes = 0;

    GameObject game_over_panel;
    GameObject victory_panel;
    // Use this for initialization
    void Start()
    {
        game_over_panel = GameObject.FindGameObjectWithTag("GameOver");
        game_over_panel.SetActive(false);
        victory_panel = GameObject.FindGameObjectWithTag("Victory");
        victory_panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (game_over && !victory)
        {
            game_over_panel.SetActive(true);
            Time.timeScale = 0.0f;
        }
        else if(!game_over && !victory)
        {
            timer += Time.deltaTime;
            if(timer >= 60.0f)
            {
                minutes++;
                timer -= 60.0f;
            }
        }
        else if(!game_over && victory)
        {
            victory_panel.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    public void Accelerate_Time()
    {
        if (game_over == false)
        {
            if (accelerated_time == false)
            {
                accelerated_time = true;
                Time.timeScale = time_accelerator_multiplier;
            }
            else
            {
                accelerated_time = false;
                Time.timeScale = 1.0f;
            }
        }
    }

    public string getTime()
    {
        string _minutes = "";
        if (minutes > 0)
        {
            _minutes = minutes.ToString();
            _minutes = string.Concat(_minutes, ": ");
        }
        string seconds = timer.ToString("N0");
        return string.Concat(_minutes,seconds);
    }
}
