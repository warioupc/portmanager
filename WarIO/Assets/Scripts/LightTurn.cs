﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTurn : MonoBehaviour {

    public float speed = 3;
    public int dive_angle = 50;

    public float time_between_taking_money = 7;
    public int money_taken_each_time = 5;

    public float time;
    Light light_c;
	// Use this for initialization
	void Start () {
        light_c = GetComponent<Light>();
        light_c.transform.Rotate(Vector3.left, 50);
        light_c.transform.Rotate(Vector3.right, dive_angle);
        time = time_between_taking_money;

    }

    // Update is called once per frame
    void Update () {
        //light_c.transform.Rotate(Vector3.up,3);
        //light_c.transform.rotation.SetEulerAngles(Mathf.Deg2Rad * 45, Mathf.Deg2Rad * angleturning, 0);
        light_c.transform.Rotate(Vector3.left, dive_angle);
        light_c.transform.Rotate(Vector3.up,speed);
        light_c.transform.Rotate(Vector3.right, dive_angle);

        time -= Time.deltaTime;

        if (light_c.enabled && time < 0)
        {
            GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().Money -= money_taken_each_time;
            time = time_between_taking_money;
        }
        //= Quaternion.AngleAxis(30, Vector3.up);

    }
}
