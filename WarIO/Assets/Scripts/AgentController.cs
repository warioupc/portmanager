﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentController : MonoBehaviour {

    public bool local_is_day = true;
    public bool is_broken = false;
    public bool has_fish = false;
    public bool lighthouse_active = false;
    public GameObject home;
    public bool is_vehicle = false;

    public int chance_of_breaking = 1;
    public int chance_of_fishing = 1;
    bool was_broken = true;
    ParticleSystem particle;
    //UI_FollowButton buttonfollower;


	// Use this for initialization
	void Start () {
        particle = GetComponentInChildren<ParticleSystem>();
        particle.Pause();
        if (particle && !is_broken && is_vehicle)
        {
        //if (particle && !is_broken && is_vehicle && buttonfollower)
        //{
        //    buttonfollower = GetComponentInChildren<UI_FollowButton>();
        //    buttonfollower.active = false;
        //
		}
	}
	
	// Update is called once per frame
	void Update () {

        if (GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<DayCycle>().is_day)
            local_is_day = true;
        else
            local_is_day = false;

        if (GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<GameController>().lighthouse_active)
            lighthouse_active = true;
        else
            lighthouse_active = false;

        

        if (particle && is_vehicle && was_broken != is_broken)
        {
            if (is_broken)
            {
                particle.Play();
                was_broken = true;
            }
            else
            {
                particle.Pause();
                particle.Clear();
                was_broken = false;
            }
        }
    }
}
