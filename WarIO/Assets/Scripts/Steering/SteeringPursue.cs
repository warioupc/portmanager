﻿using UnityEngine;
using System.Collections;

public class SteeringPursue : SteeringAbstract {

	public float max_seconds_prediction;

    public float max_distance_prediction;

    public float max_speed_prediction;


	Move move;
    SteeringSeek seek;
    SteeringArrive arrive;

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
        seek = GetComponent<SteeringSeek>();
        arrive = GetComponent<SteeringArrive>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Steer(move.target.transform.position, move.target.GetComponent<Move>().movement);
	}

	public void Steer(Vector3 target, Vector3 velocity)
	{
        // TODO 5: Create a fake position to represent
        // enemies predicted movement. Then call Steer()
        // on our Steering Seek / Arrive with the predicted position in
        // max_seconds_prediction time
        // Be sure that arrive / seek's update is not called at the same time
        float distance_factor = (target - transform.position).magnitude/max_distance_prediction;
        float speed_factor = 0;
        if (velocity.magnitude > 0)
        {
            speed_factor = 1 - velocity.magnitude/ max_speed_prediction ;
        }
        Vector3 prediction = target + velocity * (max_seconds_prediction * distance_factor * speed_factor);

        arrive.Steer(prediction,priority);
        // TODO 6: Improve the prediction based on the distance from
        // our target and the speed we have

    }
}
