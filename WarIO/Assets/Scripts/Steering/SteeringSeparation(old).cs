﻿using UnityEngine;
using System.Collections;

//public class SteeringSeparation : SteeringAbstract {

//	public LayerMask mask;
//	public float search_radius = 5.0f;
//	public AnimationCurve falloff;

//	Move move;

//	// Use this for initialization
//	void Start () {
//		move = GetComponent<Move>();
//	}
	
//	// Update is called once per frame
//	void Update () 
//	{
//        // TODO 1: Agents much separate from each other:
//        // 1- Find other agents in the vicinity (use a layer for all agents)
//        Collider[] other_agents = Physics.OverlapSphere(move.transform.position, search_radius,mask);
//        Vector3 final_vec = Vector3.zero;
//        for(int i = 0; i<other_agents.Length;i++)
//        {
//            Vector3 distance = other_agents[i].transform.position - move.transform.position;
//            if (distance != Vector3.zero)
//            {
//                float value = distance.magnitude / search_radius;
//                float curve_value = 1.0f - falloff.Evaluate(value);
//                Vector3 escape_vec = (move.transform.position - other_agents[i].transform.position).normalized * curve_value*move.max_mov_acceleration;
//                final_vec += final_vec + escape_vec;
//            }
//        }
//        if(final_vec.magnitude > move.max_mov_acceleration)
//        {
//            final_vec = final_vec.normalized * move.max_mov_acceleration;
//        }
//        move.AccelerateMovement(final_vec,priority);
//		// 2- For each of them calculate a escape vector using the AnimationCurve
//		// 3- Sum up all vectors and trim down to maximum acceleration
//	}

//	void OnDrawGizmosSelected() 
//	{
//		// Display the explosion radius when selected
//		Gizmos.color = Color.yellow;
//		Gizmos.DrawWireSphere(transform.position, search_radius);
//	}
//}
