﻿using UnityEngine;
using System.Collections;

public class SteeringWander : SteeringAbstract {


	public float min_time_until_next_calculation = 0.25f;

    public float max_time_until_next_calculation = 2.0f;

    public float min_distance = 5;

    float time_until_next_calculation = 0;
    Vector3 target;
    public float box_measures = 75.0f;
	Move move;
    SteeringSeek seek;
  

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
        seek = GetComponent<SteeringSeek>();
        RandomTarget();
	}

	// Update is called once per frame
	void Update () 
	{
        if (time_until_next_calculation <= 0)
        {
            RandomTarget();
        }
        else
        {
            time_until_next_calculation -= Time.deltaTime;
        }

        seek.Steer(target,priority);
	}

    void RandomTarget()
    {
        Vector3 i;
        i = move.transform.position;

        Vector3 ran = Vector3.zero;

        bool upx = randomBoolean();
        bool upz = randomBoolean();

        if (upx)
        {
            ran.x = Random.Range((box_measures/2) + min_distance, box_measures);
        }
        else
        {
            ran.x = Random.Range(0, (box_measures/2) - min_distance) - box_measures/2;
        }
        
        if (upz)
        {
            ran.z = Random.Range((box_measures / 2) + min_distance, box_measures);
        }
        else
        {
            ran.z = Random.Range(0, (box_measures / 2) - min_distance) - box_measures / 2;
        }

        i += ran;

        move.target.transform.position = i;
        target = move.target.transform.position;
        time_until_next_calculation = Random.Range(min_time_until_next_calculation, max_time_until_next_calculation);
        if (i.x > 240 || i.z > 0)
        {
            RandomTarget();
        }
    }

    bool randomBoolean()
    {
    if (Random.value >= 0.5)
    {
        return true;
    }
    return false;
}
	//void OnDrawGizmosSelected() 
	//{
	//	// Display the explosion radius when selected
	//	Gizmos.color = Color.white;
	//	Gizmos.DrawWireSphere(transform.position, min_distance);
 //       Gizmos.DrawSphere(target, 20);
	//}
}
