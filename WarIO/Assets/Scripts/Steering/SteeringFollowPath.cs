﻿using UnityEngine;
using System.Collections;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class SteeringFollowPath : SteeringAbstract
{

	Move move;
	SteeringSeek seek;

    float currentRatio = 0.0f;
    public BGCcMath path;
    public BGCcMath path1;
    public BGCcMath path2;

    public float ratioSpeed = 0.1f;
    public float minDistance = 1.0f;
    public float rotation_time = 1.0f;
    float current_time = 0.0f;
    float ratioVariability = 0.01f;
    Vector3 pathTarget;
    // Use this for initialization
    void Start () {
		move = GetComponent<Move>();
		seek = GetComponent<SteeringSeek>();
        ratioSpeed = Random.Range(ratioSpeed - ratioVariability, ratioSpeed + ratioVariability);

		// TODO 2: Calculate the closest point in the range [0,1] from this gameobject to the path
	}

    private void OnEnable()
    {
        move = GetComponent<Move>();
        seek = GetComponent<SteeringSeek>();
        ratioSpeed = Random.Range(ratioSpeed - ratioVariability, ratioSpeed + ratioVariability);

        pathTarget = path.CalcPositionByClosestPoint(move.transform.position);
        seek.Steer(pathTarget, priority);
    }
    // Update is called once per frame
    void Update () 
	{

            if ((move.transform.position - pathTarget).magnitude < minDistance)
            {
                currentRatio += ratioSpeed;
                if (currentRatio > 1.0f)
                    currentRatio = 0.0f;
            print("changing pathTarget");
            pathTarget = path.CalcPositionByDistanceRatio(currentRatio);   
            }
        seek.Steer(pathTarget, priority);

    }

	void OnDrawGizmosSelected() 
	{

		if(isActiveAndEnabled)
		{
			// Display the explosion radius when selected
			Gizmos.color = Color.green;
            Gizmos.DrawSphere(pathTarget, 100);
			// Useful if you draw a sphere on the closest point to the path
		}

	}
}
