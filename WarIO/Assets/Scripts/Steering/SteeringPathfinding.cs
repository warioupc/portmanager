﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class SteeringPathfinding : SteeringAbstract
{

    Move move;
    SteeringSeek seek;
    public float minDistance = 1.0f;

    public NavMeshPath path;
    int currentCornerIndex = 0;
    Vector3 pathTarget;

    public bool finished = false;
    public bool generate_path = true;

    private void Awake()
    {
        path = new NavMeshPath();
        move = GetComponent<Move>();
        seek = GetComponent<SteeringSeek>();
    }
    // Use this for initialization
    void Start()
    {

        path = new NavMeshPath();
        move = GetComponent<Move>();
        seek = GetComponent<SteeringSeek>();


    }

    private void OnEnable()
    {
        move = GetComponent<Move>();
        seek = GetComponent<SteeringSeek>();

    }
    // Update is called once per frame
    void Update()
    {
        if (move.target)
        {
            if (generate_path)
                GeneratePath();
            else
            {
                if (Vector3.Distance(pathTarget, move.transform.position) <= minDistance && !finished)
                {

                    if (currentCornerIndex >= path.corners.Length - 1)
                        finished = true;

                    else
                    {
                        currentCornerIndex += 1;
                        pathTarget = path.corners[currentCornerIndex];
                    }



                }

                seek.Steer(pathTarget, priority);
            }
        }
    }

    public void GeneratePath()
    {
        generate_path = false;
        finished = false;
        path.ClearCorners();

        currentCornerIndex = 0;
        if (move.target)
        {
            if (NavMesh.CalculatePath(transform.position, move.target.transform.position, 1, path))
            {
                pathTarget = path.corners[currentCornerIndex];
            }
        }
    }


    private void OnDrawGizmosSelected()
    {
        if (isActiveAndEnabled && Application.isPlaying)
        {
            //    for (int i = 0; i < path.corners.Length; i++)
            //    {
            //        Gizmos.color = Color.green;
            //        if (currentCornerIndex != i)
            //            Gizmos.DrawSphere(path.corners[i], 100.0f);
            //        else
            //        {
            //            Gizmos.color = Color.yellow;
            //            Gizmos.DrawSphere(path.corners[i], 100.0f);
            //        }
            //    }
            //}
        }

    }
}
