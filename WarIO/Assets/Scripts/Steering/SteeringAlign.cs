﻿using UnityEngine;
using System.Collections;

public class SteeringAlign : SteeringAbstract {

	public float min_angle = 0.01f;
	public float slow_angle = 0.1f;
	public float time_to_target = 0.1f;

	Move move;

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
	}

	// Update is called once per frame
	void Update () 
	{
        // TODO 7: Very similar to arrive, but using angular velocities
        // Find the desired rotation and accelerate to it
        // Use Vector3.SignedAngle() to find the angle between two directions  
        float angle = Vector3.SignedAngle(move.movement,move.target.transform.position, Vector3.up);
        float abs_angle = Mathf.Abs(angle);
        
        if (Mathf.Abs(angle) < min_angle)
        {
            move.SetRotationVelocity(0.0f,priority);
        }
        else
        {
            float rotation = move.max_rot_velocity;

            if(abs_angle < slow_angle)
            {
                rotation += abs_angle / slow_angle;
            }

            float rotation_acceleration = rotation / time_to_target;
            Mathf.Clamp(rotation_acceleration, -move.max_rot_acceleration, move.max_rot_acceleration);
            move.AccelerateRotation(rotation_acceleration,priority);
        }
    }
}
