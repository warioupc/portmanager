﻿using UnityEngine;
using System.Collections;

public class KinematicSeek : SteeringAbstract {

	Move move;

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
	}
	
	// Update is called once per frame
	void Update () 
	{
        // TODO 5: Set movement velocity to max speed in the direction of the target
        move.SetMovementVelocity((move.target.transform.position - transform.position).normalized * move.max_mov_velocity,priority);
        // Remember to enable this component in the inspector
    }
}
