﻿using UnityEngine;
using System.Collections;

public class KinematicWander : SteeringAbstract {

	public float max_angle = 0.5f;
    public Vector3 previous_direction = new Vector3(1, 0, 1);
    public float direction_change_delay = 5;
    private float timer;

	Move move;

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
        timer = 0;
	}

	// number [-1,1] values around 0 more likely
	float RandomBinominal()
	{
		return Random.value - Random.value;
	}
	
	// Update is called once per frame
	void Update () 
	{
        timer -= Time.deltaTime;
        if (timer <= 0.0f)
        {
            // TODO 9: Generate a velocity vector in a random rotation (use RandomBinominal) and some attenuation factor
            previous_direction = Quaternion.AngleAxis((max_angle * (RandomBinominal()) * Mathf.Rad2Deg), Vector3.up) * previous_direction;
        
            timer = direction_change_delay;
        }
        move.SetMovementVelocity(previous_direction * move.max_mov_velocity,priority);
    }
}
