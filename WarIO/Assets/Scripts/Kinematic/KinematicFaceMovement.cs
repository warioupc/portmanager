﻿using UnityEngine;
using System.Collections;

public class KinematicFaceMovement : SteeringAbstract {

	Move move;

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
	}
	
	// Update is called once per frame
	void Update () {
        // TODO 7: rotate the whole tank to look in the movement direction
        if(!move.rotating)
            Rotate(move.movement, transform.forward);
    }

    public void Rotate(Vector3 _current, Vector3 _target)
    {
        float target = Mathf.Rad2Deg * Mathf.Atan2(_current.x, _current.z);
        float current = Mathf.Rad2Deg * Mathf.Atan2(_target.x, _target.z);

        float rotation = Mathf.DeltaAngle(target, current);

        if (Mathf.Abs(rotation) < move.min_rotation_angle)
            move.SetRotationVelocity(0.0f, priority);
        else
            move.SetRotationVelocity(-rotation, priority);

    }
}
