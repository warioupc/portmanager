﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayBoatState : MonoBehaviour {

    public Sprite sprite_hasfish;
    public Sprite sprite_isfishing;
    public Sprite sprite_broken;
    public Sprite no_sprite;

    public Sprite BoatIsOkey;
    public Sprite BoatIsNotOkey;

    public Image image_state;
    public Image boat_exists;


    public GameObject Boat;

    ChangeSprite change;

    // Use this for initialization
    void Start () {
        change = Boat.GetComponent<ChangeSprite>();

	}
	
	// Update is called once per frame
	void Update () {
        if (Boat.activeSelf)
        {
            var tempColor = boat_exists.color;
            tempColor.a = 1;
            boat_exists.color = tempColor;
            boat_exists.sprite = BoatIsOkey;
        }
        else
        {
            var tempColor = boat_exists.color;
            tempColor.a = 0;
            boat_exists.color = tempColor;
            boat_exists.sprite = BoatIsNotOkey;

            tempColor = image_state.color;
            tempColor.a = 0;
            image_state.color = tempColor;

            image_state.sprite = sprite_broken;
        }
        //now we chose which is the image on the left
        if (Boat.activeSelf)
        {
            if (Boat.GetComponent<AgentController>().is_broken)
            {
                SetSprite_isbroken();
            }
            else if (change.image.sprite == sprite_hasfish)
            {
                SetSprite_hasfish();
            }
            else if (change.image.sprite == sprite_isfishing)
            {
                SetSprite_isfishing();
            }


        }
	}


    public void SetSprite_hasfish()
    {
        var tempColor = image_state.color;
        tempColor.a = 1;
        image_state.color = tempColor;

        image_state.sprite = sprite_hasfish;
    }

    public void SetSprite_isfishing()
    {
        var tempColor = image_state.color;
        tempColor.a = 1;
        image_state.color = tempColor;

        image_state.sprite = sprite_isfishing;
    }

    public void SetSprite_isbroken()
    {
        var tempColor = image_state.color;
        tempColor.a = 1;
        image_state.color = tempColor;

        image_state.sprite = sprite_broken;
    }
}
