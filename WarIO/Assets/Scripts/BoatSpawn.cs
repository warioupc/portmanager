﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatSpawn : MonoBehaviour {

    public float spawn_rate = 1;
    public int max_boats = 10;
    public GameObject[] day_spawns;
    public GameObject[] night_spawns;
    GameObject[] boats;
    public GameObject boat_prefab;
     
    int num_boats;
    public float time_until_next_possible_spawn;
    float current_time;

    DayCycle day_cycle;
	// Use this for initialization
	void Start () {
        day_cycle = GetComponent<DayCycle>();
        day_spawns = new GameObject[max_boats];
        day_spawns = GameObject.FindGameObjectsWithTag("Day_Spawn");
        night_spawns = new GameObject[max_boats];
        night_spawns = GameObject.FindGameObjectsWithTag("Night_Spawn");
        boats = new GameObject[max_boats];

        num_boats = 0;
        //seconds
	}
	
	// Update is called once per frame
	void Update () {

        current_time += Time.deltaTime;
        if (day_cycle.is_day)
        {
            if (time_until_next_possible_spawn <= current_time && num_boats < max_boats)
            {
                
                GameObject current_boat = Instantiate(boat_prefab, day_spawns[num_boats].transform.position, boat_prefab.transform.rotation);
                current_boat.GetComponent<Move>().target = night_spawns[num_boats];
                boats[num_boats] = current_boat;
                current_time = 0;
                num_boats++;
            }
        }

        if(!day_cycle.is_day)
        {
            for(int i = 0; i < boats.Length-1; i++)
            {
                if(boats[i].GetComponent<SteeringPathfinding>().finished)
                {
                    Destroy(boats[i]);
                }
            }
        }

	}

    public void ChangeBoatsTargets()
    {
        for (int i = 0; i < boats.Length; i++)
        {
            print("doint the thing");
            boats[i].GetComponent<Move>().target = day_spawns[i];
            boats[i].GetComponent<SteeringPathfinding>().GeneratePath();
        }
    }

    public void EliminateBoats()
    {
        for (int i = 0; i < boats.Length; i++)
        {
            Destroy(boats[i]);
            num_boats = 0;
        }
    }
}
