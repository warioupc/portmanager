﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repair : MonoBehaviour {

    Dock_Controller dock_Controller;
    AgentController agentController;

    public void  Repair_vehicle(GameObject @_object)
    {
        agentController = _object.GetComponent<AgentController>();
        if(agentController)
            agentController.is_broken = false;

        dock_Controller = _object.GetComponent<Dock_Controller>();
        if (dock_Controller)
            dock_Controller.needs_repair = false;
    }
}
