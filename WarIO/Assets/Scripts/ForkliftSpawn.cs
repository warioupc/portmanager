﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class ForkliftSpawn : MonoBehaviour {

    // Use this for initialization
    public float spawn_rate = 1;
    public int max_forklifts = 10;
    GameObject[] forklifts;
    public GameObject forklift_prefab;
    public GameObject spawn;

    public BGCcMath day_path;
    public BGCcMath night_path;

    bool using_day_path = false;

    int num_forklifts = 0;
    public float time_until_next_possible_spawn;
    float current_time;

    DayCycle day_cycle;
    void Start () {
        day_cycle = GetComponent<DayCycle>();
        forklifts = new GameObject[max_forklifts];

	}
	
	// Update is called once per frame
	void Update () {
        current_time += Time.deltaTime;
        if(time_until_next_possible_spawn <= current_time && num_forklifts < max_forklifts)
        {
            GameObject current_forklift = Instantiate(forklift_prefab, spawn.transform.position, forklift_prefab.transform.rotation);
            current_forklift.GetComponent<SteeringFollowPath>().path = day_path;
            forklifts[num_forklifts] = current_forklift;
            current_time = 0;
            num_forklifts++;
        }
        else if(day_cycle.is_day && num_forklifts == max_forklifts && using_day_path == false)
        {
            for (int i = 0; i < forklifts.Length; i++)
            {
                forklifts[i].GetComponent<SteeringFollowPath>().path = day_path;
            }
            using_day_path = true;
        }
        if(!day_cycle.is_day && using_day_path)
        {
            print("switching curves");
            for(int i = 0; i<forklifts.Length;i++)
            {
                forklifts[i].GetComponent<SteeringFollowPath>().path = night_path;
            }
            using_day_path = false;
        }
		
	}
}
