﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Display_variable_Money: MonoBehaviour {

    Text text;
    public float value;

    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        text.text = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().Money.ToString();
    }
}

