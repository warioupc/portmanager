﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycle : MonoBehaviour
{
     public bool is_day = true;
     public bool is_changing = false;

    public float cycle_time = 60.0f;//total seconds of the cycle
    public float change_time = 10.0f;

    float current_time = 0;
    Light light;

    Resource_manager_script Resource_Manager_Script;

    // Use this for initialization
    void Start()
    {
        light = GetComponent<Light>();
        Resource_Manager_Script = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
    }

    // Update is called once per frame
    void Update()
    {
        current_time += Time.deltaTime;

        if(current_time >= cycle_time && !is_changing && is_day)
        {
            current_time = 0;
            is_changing = true;
        }
        else if(current_time < change_time && is_changing && is_day)
        {
            light.intensity = 1 - current_time / change_time;
        }
        else if(current_time >= change_time && is_changing && is_day)
        {
            current_time = 0;
            is_day = false;
            is_changing = false;
            light.intensity = 0.0f;
        }
        else if(current_time >= cycle_time && !is_changing && !is_day)
        {
            current_time = 0;
            is_changing = true;
        }
        else if (current_time < change_time && is_changing && !is_day)
        {
            light.intensity = current_time / change_time;
        }
        else if (current_time >= change_time && is_changing && !is_day)
        {
            current_time = 0;
            is_day = true;
            is_changing = false;
            light.intensity = 1.0f;
            //here
            Resource_Manager_Script.NewDay();
       
        }
    }
}
