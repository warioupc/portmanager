﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : GameController {

    bool used = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ToggleUsed()
    {
        used = !used;
    }

    bool BeingUsed()
    {
        return used;
    }
}
