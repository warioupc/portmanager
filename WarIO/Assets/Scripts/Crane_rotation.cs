﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crane_rotation : MonoBehaviour {

    public bool rotate = false;
    public float rotation_speed = 1.0f;
    Dock_Controller dock_controller;
	// Use this for initialization
	void Start () {
        dock_controller = gameObject.GetComponentInParent<Dock_Controller>();
	}
	
	// Update is called once per frame
	void Update () {
        if(!dock_controller)
            dock_controller = gameObject.GetComponentInParent<Dock_Controller>();

        if (dock_controller.has_materials == true)
            rotate = true;
        else
            rotate = false;

        if(rotate)
        {
            transform.rotation *= Quaternion.AngleAxis(-rotation_speed * Time.deltaTime, Vector3.up);
        }
        else if(Mathf.Abs(transform.rotation.eulerAngles.y) > 1)
            transform.rotation *= Quaternion.AngleAxis(-rotation_speed * Time.deltaTime, Vector3.up);

    }
}
