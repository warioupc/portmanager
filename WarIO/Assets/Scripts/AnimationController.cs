﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    Animator m_Animator;
    Move move;

    // Use this for initialization
    void Start () {
		move = gameObject.GetComponent<Move>();
        m_Animator = gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        if (move.movement.magnitude > 2)
        {
            m_Animator.SetBool("moving", true);
        }
        else
        {
            m_Animator.SetBool("moving", false);
        }

    }
}
