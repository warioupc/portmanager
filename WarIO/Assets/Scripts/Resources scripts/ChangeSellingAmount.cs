﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSellingAmount : MonoBehaviour {

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Change(int amount)
    {
        int max_selling_amount = 5 + GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Buyer_happiness_controller>().buyer_happiness / 5;

        Resource_manager_script r = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();

        Camera.main.GetComponent<AudioController>().PlayMenuClickSound();

        if (r.selling_amount + amount <= r.Materials)
        {
            if (amount > 0 &&amount + GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount < max_selling_amount)
            {
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount += amount;
            }
            else if (GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount >= -amount)
            {
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount += amount;
            }
        }

    }
}
