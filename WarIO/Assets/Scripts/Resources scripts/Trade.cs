﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trade : MonoBehaviour {

    public float time_between_trades = 5;
    public float timer;

    AudioSource audioData;
    public int expected_price = 8;

    void Start()
    {
        timer = 0;
        audioData = GetComponent<AudioSource>();
        audioData.Stop();
    }

    // Update is called once per frame
    void Update () {
        timer -= Time.deltaTime;

	}

    public void Trade_with_current()
    {
        if (timer <= 0 )
        {
            audioData.Play(0);
            if (GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().Materials >= GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount)
            {
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().Materials -= GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount;
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().Money += GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price * GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount;

                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Buyer_happiness_controller>().buyer_happiness -= ((GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price - expected_price) * GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount)/5;
                timer = time_between_trades;

                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().selling_amount = 0;
            }
        }

    }
}
