﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePrice : MonoBehaviour {

    public int max_price = 16;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Change(int amount)
    {
        if (GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price + amount <= max_price && GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price + amount >= 0)
        {
            Camera.main.GetComponent<AudioController>().PlayMenuClickSound();
            if (amount > 0)
            {
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price += amount;
            }
            else if (GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price >= -amount)
            {
                GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>().sell_price += amount;
            }
        }

    }
}
