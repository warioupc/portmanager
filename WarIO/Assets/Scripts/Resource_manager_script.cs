﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource_manager_script : MonoBehaviour {

    public int Money = 50;
    public int Materials = 10;

    public int sell_price = 10;
    public int selling_amount = 5;

    BuyWorker buy_worker;
    BuyForklift buy_forklift;

    public int worker_salary = 10;
    public int forklift_salary = 5;

    public int victory_money = 1000;
    GameController game_controller;

	// Use this for initialization
	void Start () {
        buy_worker = GameObject.FindGameObjectWithTag("BuyWorker").GetComponent<BuyWorker>();
        buy_forklift = GameObject.FindGameObjectWithTag("BuyForklift").GetComponent<BuyForklift>();
        game_controller = GetComponent<GameController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Money >= victory_money)
            game_controller.victory = true;
	}

    public void NewDay()
    {
        int num_workers = buy_worker.num_of_workers();
        int num_forklifts = buy_forklift.num_of_forklifts();

        Materials -= (num_workers * worker_salary + num_forklifts * forklift_salary);

        if (Materials < 0)
        {
            game_controller.game_over = true;
        }
    }
}
