﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_FollowText : MonoBehaviour {

    public Text object_to_follow;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {

        object_to_follow.transform.position = (this.transform.position);
    }
}
