﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buy_Boats : MonoBehaviour {

    public int boat_price = 100;
    public Dock_Controller dock_controller;
    Resource_manager_script game_controller;
    int boats_active = 0;
	// Use this for initialization
	void Start () {
        game_controller = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BuyBoat()
    {
        Camera.main.GetComponent<AudioController>().PlayBoatSound();
        if(game_controller.Money >= boat_price)
        {
            if (dock_controller.ActivateBoat())
                game_controller.Money -= boat_price;
        }
    }
}
