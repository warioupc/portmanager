﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSprite : MonoBehaviour {

    // Use this for initialization
    public Sprite sprite_hasfish;
    public Sprite sprite_isfishing;
    public Sprite sprite_isbroken;
    public Sprite no_sprite;

    public Image image;

	void Start () {

        SetSprite_hasfish();
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 newpos;
        newpos.x = this.transform.position.x;
        newpos.y = this.transform.position.y + 65;
        newpos.z = this.transform.position.z - 50;
        //image.transform.position =  Vector3(this.transform.position.x, this.transform.position.y + 20, this.transform.position.z);
        image.transform.position = newpos;
    }

    public void SetSprite_hasfish()
    {
        var tempColor = image.color;
        tempColor.a = 1;
        image.color = tempColor;

        image.sprite = sprite_hasfish;
    }

    public void SetSprite_isfishing()
    {
        var tempColor = image.color;
        tempColor.a = 1;
        image.color = tempColor;

        image.sprite = sprite_isfishing;
    }

    public void SetSprite_isbroken()
    {
        var tempColor = image.color;
        tempColor.a = 1;
        image.color = tempColor;

        image.sprite = sprite_isbroken;
    }

    public void SetSprite_none()
    {
        var tempColor = image.color;
        tempColor.a = 0;
        image.color = tempColor;
    }
}
