﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIntroBoat : MonoBehaviour {

    public float speed = 1.0f;
    public float limit = -230.0f;
    public float starting_point = 200.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.position.x > limit)
        {
            Vector3 new_x = new Vector3(speed * Time.deltaTime, 0, 0);
            transform.position -= new_x;
        }
        else
            transform.position = new Vector3(starting_point, transform.position.y, transform.position.z);
	}
}
