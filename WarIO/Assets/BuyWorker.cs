﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyWorker : MonoBehaviour {

    public GameObject[] workers;
    Resource_manager_script resource_manager;
    public int price;
    // Use this for initialization
    void Start()
    {
        resource_manager = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
        workers = GameObject.FindGameObjectsWithTag("Worker");
        for (int i = 1; i < workers.Length; i++)
        {
            workers[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Buy_Worker()
    {
        if (resource_manager.Money >= price)
        {

            for (int i = 0; i < workers.Length; i++)
            {
                if (workers[i].activeSelf == false)
                {
                    Camera.main.GetComponent<AudioController>().PlayWorkerSound();
                    workers[i].SetActive(true);
                    resource_manager.Money -= price;
                    break;
                }
            }
        }
    }

    public int num_of_workers()
    {
        int ret = 0;
        for (int i = 0; i < workers.Length; i++)
        {
            if (workers[i].activeSelf == true)
            {
                ret++;
            }
        }
        return ret;
    }
}
