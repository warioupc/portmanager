﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMoney : MonoBehaviour {

    Text text;
    Resource_manager_script resource_manager;
    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
        resource_manager = GameObject.FindGameObjectWithTag("Game_Controller").GetComponent<Resource_manager_script>();
    }

    // Update is called once per frame
    void Update()
    {
        string _text = "You reached: \n";
        string current_money = resource_manager.Money.ToString();
        string max_money = resource_manager.victory_money.ToString();
        text.text = string.Concat(_text, current_money,"$", " out of ", max_money,"$");
    }
}
