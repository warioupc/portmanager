﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class background_audio : MonoBehaviour {

    AudioSource m_AudioSource;

    void Start()
    {
        //Fetch the AudioSource component of the GameObject (make sure there is one in the Inspector)
        m_AudioSource = GetComponent<AudioSource>();
        //Stop the Audio playing
        m_AudioSource.loop = true;
        //Call the PlayButton function when you click this Button
    }

    void Update()
    {
        //Turn the loop on and off depending on the Toggle status
    }
}
