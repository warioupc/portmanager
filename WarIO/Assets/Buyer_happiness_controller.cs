﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buyer_happiness_controller : MonoBehaviour {

    public int buyer_happiness = 50;

    public Sprite Happy;
    public Sprite Content;
    public Sprite Normal;
    public Sprite Disapointed;
    public Sprite Angry;

    public Image image;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (buyer_happiness > 80)
        {
            image.sprite = Happy;
        }
        else if (buyer_happiness > 60)
        {
            image.sprite = Content;
        }
        else if (buyer_happiness > 40)
        {
            image.sprite = Normal;
        }
        else if (buyer_happiness > 20)
        {
            image.sprite = Disapointed;
        }
        else
        {
            image.sprite = Angry;
        }
    }
}
