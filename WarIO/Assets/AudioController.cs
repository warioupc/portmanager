﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    public AudioSource source;
    public AudioClip audio_materials;
    public AudioClip audio_menu_click;
    public AudioClip audio_lighthouse_toggle;
    public AudioClip audio_boat;
    public AudioClip audio_forklift;
    public AudioClip audio_worker;

    // Use this for initialization
    void Start () {
        //source = GetComponent<AudioSource>();
        //source.Stop();
        
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void PlayMaterialsSound()
    {
        
        source.clip = audio_materials;
        //source.PlayOneShot(audio_materials);
        source.Play(0);
    }

    public void PlayMenuClickSound()
    {
        source.clip = audio_menu_click;
        //source.PlayOneShot(audio_menu_click);

        source.Play(0);
    }

    public void PlayLighthouseToggle()
    {
        source.clip = audio_lighthouse_toggle;
        //source.PlayOneShot(audio_lighthouse_toggle);

        source.Play(0);
    }

    public void PlayBoatSound()
    {

        source.clip = audio_boat;
        //source.PlayOneShot(audio_materials);
        source.Play(0);
    }

    public void PlayForkliftSound()
    {
        source.clip = audio_forklift;
        //source.PlayOneShot(audio_menu_click);

        source.Play(0);
    }

    public void PlayWorkerSound()
    {
        source.clip = audio_worker;
        //source.PlayOneShot(audio_lighthouse_toggle);

        source.Play(0);
    }
}
