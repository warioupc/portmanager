Version 1.0 of the game Port Manager

Made by Hugo B� and Lori�n Portella

Bitbucket: https://bitbucket.org/warioupc/portmanager/src/master/

Webpage: https://warioupc.github.io/PortManager/

This version is a test of the behaviours implemented in class and a little prototype of how the game will look in the future

WASD are the controls for the camera so that you can obvserve what is going on better

Game areas:
-Sea: The ships come from there with the products that you have to carry into the warehouses.

-Docks: Where the forklifts take the goods from the boats to the selling point

-Port: Where the forklifts and workers are stored.

Types of AI:
-Boats: The boats spend the day at sea until they manage to gather fish. Then, they return to the docks. At night, all the boats return to the docks, but some are empty. If the lighthouse is active, the boats will keep working at night.

-Workers: They are in charge of repairing the boats

-Forklifts: The forklifts take the resources brought by the boats to the selling spot.

Economy:
Materials: Brought by the ships. They are used to pay workers daily or sell them to earn money.
Money: Used to pay the lighthouse during the night. Also used to buy more ships, workers and forklifts
Cycle:
Day/Night: During night, you need to pay to the lighthouse if you want ships to keep coming to your harbour.

